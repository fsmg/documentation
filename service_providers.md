# Third party service providers

## GitLab

[https://gitlab.com/fsmg](https://gitlab.com/fsmg) is a free account run by Brad, James and Michael. All members of the account must have 2FA enabled in GitLab.

GitLab is where we collaborate on documentation, tools, scripts and ansible playbooks.

## E-mail hosting

E-mail hosting is currently provided by James on a best effort basis.

These aliases exist:

```
contact@fsmg.org.nz mirror-operators@fsmg.org.nz
sponsorship@fsmg.org.nz mirror-operators@fsmg.org.nz
security@fsmg.org.nz mirror-operators@fsmg.org.nz
mirror-operators@fsmg.org.nz james.forman@fsmg.org.nz brad.cowie@fsmg.org.nz michael.fincham@fsmg.org.nz
```

These mailboxes exist:

```
james.forman@fsmg.org.nz
brad.cowie@fsmg.org.nz
michael.fincham@fsmg.org.nz
```

## Domain registration

This is provided as part of a sponsorship deal with [Metaname](https://metaname.net). Our Metaname username is "4mkm", and currently Michael has the credentials to log in to this, and the 2FA token.

## DNS

Provided by Catalyst. The API key is stored in `/opt/fsmg/bin/pdns-cli/conf.toml` on both servers.

## Rackspace and connectivity

REANNZ kindly provides rackspace and connectivity.

Contact information for REANNZ is available at [https://gitlab.com/fsmg/private/blob/master/reannz](https://gitlab.com/fsmg/private/blob/master/reannz).

## Monitoring and alerting

Currently monitoring is through James's Icinga2 installation at [https://monitoring.jfnet.nz/](https://monitoring.jfnet.nz/). James sponsors the alerting for FSMG.

There is also grafana/prometheus VM [grafana.fsmg.org.nz](https://grafana.fsmg.org.nz)
which is hosted on a VM sponsored by [sitehost](https://sitehost.nz).
This VM can be managed from the [sitehost control panel](https://cp.sitehost.nz) using your [firstname.lastname]@fsmg.org.nz sitehost account.
You can also SSH to this VM using the same account as the FSMG nodes.

## TLS

Let's Encrypt is used to issue https certificates.

## Website

Our website is hosted on GitLab pages.
