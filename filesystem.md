# Filesystem layout of FSMG servers

## Directories in /srv

* /srv/mirror, webroot of https://mirror.fsmg.org.nz
* /srv/website, webroot of https://fsmg.org.nz
* /srv/stats, contains files used to generate https://mirror.fsmg.org.nz/.fsmg/nodes/&lt;nodename&gt;/, e.g. https://mirror.fsmg.org.nz/.fsmg/nodes/hlzmel/

## Disk layout

The disks are in a RAID 10 configuration, and the partition layout on each disk is:

````console
wlglam:/opt/fsmg/doc# sfdisk -d /dev/sda
label: gpt
label-id: A7663462-ABFD-41C5-A420-3FC879FBF82A
device: /dev/sda
unit: sectors
first-lba: 34
last-lba: 11721045134

/dev/sda1 : start=        2048, size=       32768, type=21686148-6449-6E6F-744E-656564454649, uuid=50792B1B-14DB-478D-8440-58FFB2686203, name="wlglam-bios-0", attrs="LegacyBIOSBootable"
/dev/sda2 : start=       34816, size=     1048576, type=A19D880F-05FC-4D3B-A006-743F0F84911E, uuid=03FC4855-30E8-47B3-BB3F-918A3B9AF32A, name="wlglam-raid-boot-0"
/dev/sda3 : start=     1083392, size=    67108864, type=A19D880F-05FC-4D3B-A006-743F0F84911E, uuid=4B39CA33-5848-4F63-BD30-20D70249BBA7, name="wlglam-raid-root-0"
/dev/sda4 : start=    68192256, size= 11652852879, type=A19D880F-05FC-4D3B-A006-743F0F84911E, uuid=22CA96FC-1209-4553-AFEA-C2C8DD136E12, name="wlglam-raid-storage-0"
````

If you use this dump to create a new disk be sure to change the volume names and remove the UUIDs.

md layout:

````console
wlglam:/opt/fsmg/doc# cat /proc/mdstat 
Personalities : [raid10] [raid1] [linear] [multipath] [raid0] [raid6] [raid5] [raid4] 
md1 : active raid1 sdb3[1] sdc3[2] sda3[0] sdd3[3]
      33521664 blocks super 1.2 [4/4] [UUUU]
      
md0 : active raid1 sdb2[1] sda2[0] sdc2[2] sdd2[3]
      523712 blocks super 1.2 [4/4] [UUUU]
      
md2 : active raid10 sdb4[1] sdc4[2] sda4[0] sdd4[3]
      11652589568 blocks super 1.2 512K chunks 2 far-copies [4/4] [UUUU]
      bitmap: 0/87 pages [0KB], 65536KB chunk

unused devices: <none>
````

LVM layout:

````console
hlzmel:/home/fincham# pvs
  PV         VG             Fmt  Attr PSize  PFree 
  /dev/md1   hlzmel-system  lvm2 a--  31.96g 15.79g
  /dev/md2   hlzmel-storage lvm2 a--  10.85t  1.85t
hlzmel:/home/fincham# lvs
  LV     VG             Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  mirror hlzmel-storage -wi-ao----  9.00t                                                    
  root   hlzmel-system  -wi-ao---- 14.31g                                                    
  swap_1 hlzmel-system  -wi-ao----  1.86g    
````

Mountpoints:

````console
wlglam:/opt/fsmg/doc# mount | grep wlglam
/dev/mapper/wlglam--system-root on / type ext4 (rw,noatime,errors=remount-ro,data=ordered)
/dev/mapper/wlglam--storage-mirror on /srv/mirror type xfs (rw,noatime,attr2,inode64,sunit=1024,swidth=4096,usrquota)
````

`usrquota` is used to keep quick track of how much disk space each mirror uses, as each mirror has its own user.