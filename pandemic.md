# Plans relating to the 2020 SARS-CoV-2 pandemic

While FSMG is important to the operation of the Internet in New Zealand, no aspect of FSMG's operation is worth risking the health of our volunteers or the staff of our critical service providers.

Our co-location and network supplier REANNZ has been designated an essential business and so may continue to operate during New Zealand's COVID-19 response. Any work therefore undertaken on FSMG's physical infrastructure would be at the direction of REANNZ during at least COVID-19 alert levels 3 and 4.

In order to avoid the potential for undue disruption during COVID-19 alert levels 3 and 4 FSMG has undertaken a "change freeze" to minimise the chance that physical intervention with equipment is required.

This means:

- Only critical security patches will be installed. Michael is reviewing Debian security advisories daily to ensure critical vulnerabilities are known about in a timely fashion.
- Only minor configuration changes should be made. All configuration changes that are made must be subject to a peer review process by at least one other operator, and discussed within the operator group.

In general any work that requires physical maintenance of the servers would be discussed with REANNZ to ensure that no un-due demands are made on them. Only if REANNZ is 100% comfortable with all the safety aspects of any work would that work go ahead. We would much rather run with a single active site for longer than put anyone's personal safety at risk.

In the event that FSMG operators need to be physically present during any maintenance, they would observe these precautions:

- Ensure that the work is absolutely necessary
- Use hand sanitiser or thorough hand washing with soap both immediately before entering and after leaving any shared spaces (e.g. co-location facilities, public transport etc)
- Stay at least 2 metres away from any other people involved in the procedure
- Where it is not possible to strictly remain 2 metres away from other people the operator should not remain in this position any longer than is necessary, and no longer than 15 minutes
- Any other requirements suited to the site or other people involved in the procedure to minimise health risk

In general we must follow the New Zealand government's requirements for various COVID-19 alert levels, as outlined on https://covid19.govt.nz/.

Any changes to this plan or discussions regarding it should be undertaken with at least Brad, James and Michael.

## Projects waiting on a return to lower COVID-19 alert level

- Kernel and release upgrades to Debian Buster
- Possible installation of a solid state cache drive
