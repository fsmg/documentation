# FSMG website

## fsmg.org.nz

[fsmg.org.nz](https://fsmg.org.nz) is a pelican website hosted on GitLab pages
infrastructure.

[www.fsmg.org.nz](https://www.fsmg.org.nz) is pointed at the FSMG mirror nodes
and apache rules are used to redirect to [fsmg.org.nz](https://fsmg.org.nz).

### Deploying changes

Deploying changes to our website is easy and automated.

  1. The first step is to raise your change as a merge request on our
     [website git repo](https://gitlab.com/fsmg/website/).
  2. This will trigger a CI job that builds the website and deploys it to a
     staging environment. When this CI job finishes after ~1 minute you can
     click the `View app` button which will appear on the the merge request page.
  3. When happy with your change, merge it into the repo and a CI will
     automatically deploy your change to [fsmg.org.nz](https://fsmg.org.nz).

### GitLab configuration

The configuration for our GitLab pages instance can be found
[here](https://gitlab.com/fsmg/website/pages).

### GitLab outages

In the event of a GitLab pages outage, the mirror nodes are configured to be
able to serve our website.

An older build of our website is stored on each mirror node at `/srv/website`.
If you wish to serve a newer version of the site than what is there, a backup
of the website git directory can be found at `/opt/fsmg/git/website/`, just
build this according to the instructions in
`/opt/fsmg/git/website/.gitlab-ci.yml` and put the new build in `/srv/website`.

To move the website back to our infrastructure from GitLab, simply update the
DNS records to point back to us:

```
fsmg.org.nz.        IN      A       163.7.134.113
fsmg.org.nz.        IN      AAAA    2404:138:4000::1
```

When GitLab is working again point the A record for fsmg.org.nz. back to
whatever A record `fsmg.gitlab.io` is resolving to and delete the AAAA record.
