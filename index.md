# Infrastructure documentation

This set of documents is the operator's manual for the FSMG infrastructure.

Contents
--------

* [Pandemic plans](pandemic.md)

* [Contacting the FSMG operators](operators.md)
* [Hosted equipment](equipment.md)
* [External service providers](service_providers.md)


* [Sync users](sync_users.md)
* [Network services](services.md)
* [Website](website.md)
* [File structure](filesystem.md)
* [Configuration management](ansible.md)
* [Monitoring](monitoring.md)
* [Replication](replication.md)
* [Anycast](anycast.md)
* [Hardware spares](spares.md)
* [Is this a necessary mirror?](decisions.md)
* [Patching process and schedule](patching.md)
* [Security considerations](security.md)
* [This documentation](documentation.md)

Quick references
----------------

* [ftpsync documentation](https://salsa.debian.org/mirror-team/archvsync/)
