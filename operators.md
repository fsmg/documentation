# Contacting the FSMG operators

## The operators

The operators are volunteers. Contact details for the operators are available at [https://gitlab.com/fsmg/private/blob/master/operator-contacts](https://gitlab.com/fsmg/private/blob/master/operator-contacts).

Users of the mirror may contact [mirror-operators@fsmg.org.nz](mailto:mirror-operators@fsmg.org.nz) for support.

## IRC

There is an official IRC channel on freenode at [#fsmg](ircs://irc.freenode.net/fsmg). Michael, Brad, James and Tom have channel ops and Michael is channel owner.