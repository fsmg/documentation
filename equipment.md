# Hosted equipment

FSMG has two servers, one in Hamilton and one in Wellington.

These machines were donated by our [sponsors](https://fsmg.org.nz/pages/sponsors.html).

## Wellington (wlglam)

wlglam is a Sun Microsystems SunFire X2270 1U server with
12 Intel(R) Xeon(R) X5660 CPU cores and 32GiB of RAM.
It has the serial number 0328MSL-0921660383.

## Hamilton (hlzmel)

hlzmel is a Supermicro SYS-5018R-M 1U server with
12 Intel(R) Xeon(R) E5-2620 v3 CPU cores and 32GiB of RAM.
It has the serial number A16231415601793.

## Disks

Each machine has four 6TB disks. Originally these were WD WD60EFRX-68L0BN1 disks but over time we have acquired a more heterogenous mix as drives were replaced.

In "wlglam":

````
/dev/sda: WD-WX11D278Z5S5
/dev/sdb: WD-WX11D278ZPYR
/dev/sdc: WD-WX11D278ZU9J
/dev/sdd: WD-WX12D53JR45H
````

In "hlzmel":

````
/dev/sda: WD-C816Z21G
/dev/sdb: WD-WX12D53JR2VU
/dev/sdc: WD-C80R9W1G
/dev/sdd: WD-WX11D278Z83J
````

Spare disks [are held in Wellington and Hamilton](spares.md).

More information about the disk configuration is available on the [filesystems](filesystem.md) page.

## RAM

In "wlglam" we have one Micron 36KSF1G72PZ-1G4M1 8GiB DDR3-1333 ECC Registered DIMM and three equivalent Kingston DIMMs.

In "hlzmel" we have four Micron 18ASF1G72PZ-2G1A2 8GiB DDR4-2133 Registered DIMMs.

## Labelling

The machines are very clearly labeled on the front, back and top with FSMG contact details.

## Locations

The "wlglam" node is hosted in the CityLink facility on Lambton Quay in space leased by [REANNZ](service_providers.md).

The "hlzmel" node is hosted in the Vocus HNINNO facility on Melody Lane in space leased by REANNZ. Our contact there is Brad, and the REANNZ contacts.

## Hardware quirks

The "wlglam" node had a failed power LED, so this LED has been replaced with an extremely bright blue LED. You'll know it when you see it.

