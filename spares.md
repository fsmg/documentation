# Where are the spares kept?

Michael has one questionable hard drive spare in Wellington - passes self tests and SMART looks good but was unreliable under load.

Brad has two questionable spares in Hamilton but has a source for further drives as needed.

Contact them using the mirror operators contact if spare hardware is needed.